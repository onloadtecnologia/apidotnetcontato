using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiMinimal.Models
{
    public class Contato
    {
        public int id {get;set;}
        public string data {get;set;} = DateTime.Now.ToShortDateString();
        public string? nome {get;set;}
        public string? telefone {get;set;}
        public string? email {get;set;}

    }
}
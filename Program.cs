using apiMinimal.Context;
using apiMinimal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using apidotnetcontato.Authetication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using apidotnetcontato.Dtos;

var builder = WebApplication.CreateBuilder(args);
var key = Encoding.UTF8.GetBytes("minhachavesupersecreta");

// Add services to the container.
builder.Services.AddScoped<JwtAuthenticationService>();
builder.Services.AddControllers();
var conexao = builder.Configuration.GetConnectionString("conexao");
builder.Services.AddSqlite<DatabaseContext>(conexao);
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateIssuer = false,
        ValidateAudience = false
    };

});

builder.Services.AddAuthorization(options => {
    options.AddPolicy("contatos",policy => policy.RequireClaim("admin"));
    
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddAuthorization();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// app.Urls.Add("http://localhost:8000");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(x=>{
    x.AllowAnyHeader()
    .AllowAnyMethod()
    .AllowAnyOrigin();
});

app.UseAuthentication();
app.UseAuthorization();


// app.MapControllers();*********************************************************************

app.MapGet("/contatos", [Authorize] async Task<List<Contato>> (DatabaseContext db) =>
{
    var contatos = await db.contatos.ToListAsync<Contato>();
    return await Task.FromResult(contatos);
});

app.MapGet("/contatos/{id}", [Authorize] async Task<ActionResult<Contato>> (DatabaseContext db, int id) =>
{
    var contato = await db.contatos.FirstOrDefaultAsync<Contato>(x => x.id.Equals(id));
    if (contato != null)
    {
        return await Task.FromResult(contato);
    }
    else
    {
        return new NotFoundResult();
    }
});

app.MapPost("/contatos", [Authorize] async Task<List<Contato>> (DatabaseContext db, Contato ct) =>
{
    
    ct.data = DateTime.Now.ToShortDateString();
    await db.contatos.AddAsync(ct);
    int save = await db.SaveChangesAsync();
    var contatos = await db.contatos.ToListAsync<Contato>();
    return await Task.FromResult(contatos);
});

app.MapPut("/contatos/{id}", [Authorize] async Task<List<Contato>> (DatabaseContext db, int id, Contato ct) =>
{
    var contatos = await db.contatos.ToListAsync<Contato>();
    var old = await db.contatos.FirstOrDefaultAsync<Contato>(x => x.id.Equals(id));
    if (old is not null)
    {
        old.data = DateTime.Now.ToShortDateString();
        old.nome = ct.nome;
        old.telefone = ct.telefone;
        old.email = ct.email;
        db.Entry(old).State = EntityState.Modified;
        await db.SaveChangesAsync();
        return await Task.FromResult(contatos);
    }
    return await Task.FromResult(contatos);
});

app.MapDelete("/contatos/{id}", [Authorize] async Task<List<Contato>> (DatabaseContext db, int id) =>
{
    var del = await db.contatos.FirstOrDefaultAsync<Contato>(x => x.id.Equals(id));
    if (del is not null)
    {
        db.contatos.Remove(del);
        await db.SaveChangesAsync();
        return await db.contatos.ToListAsync();
    }
    else
    {
        return await db.contatos.ToListAsync();
    }
});

//*********************************************************************************************************************************

app.MapPost("/cadastro", async Task<UserDto> (DatabaseContext db, UserDto user) =>
{
   User userSave = new();
   userSave.username = user.username; 
   userSave.email = user.email; 
   userSave.password = user.password; 
   userSave.role = user.role; 
   await db.users.AddAsync(userSave);
   await db.SaveChangesAsync();   
   return await Task.FromResult(user);
 
});

app.MapPost("/login", async Task<Dictionary<string, string>> (DatabaseContext db, JwtAuthenticationService service, UserDto user) =>
{
    var findUser = await db.users.FirstOrDefaultAsync<User>(x => x.email == user.email);
    Dictionary<string, string> dicionario = new Dictionary<string, string>();

    if(findUser != null && findUser.password == user.password){
        dicionario.Add("token", service.GeneratedToken(user));
        return await Task.FromResult(dicionario);
    }else{
        dicionario.Add("status","401");
        return await Task.FromResult(dicionario);        
    }
});

app.Run();

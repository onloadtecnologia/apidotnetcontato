# Take a base image from the public Docker Hub repositories
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
# Navigate to the “/app” folder (create if not exists)
WORKDIR /app
# Copy csproj and download the dependencies listed in that file
COPY *.csproj ./
RUN ["dotnet","restore"]
RUN ["dotnet","dev-certs","https","--trust"]
# Copy all files in the project folder
COPY . ./
RUN dotnet publish -c Release -o out
# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app

COPY database.db ./

COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "apiMinimal.dll","--urls", "http://0.0.0.0:5000"]
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apidotnetcontato.Dtos
{
    public class UserDto
    {
        public string? username {get;set;} 
        public string? email {get;set;} 
        public string? password {get;set;} 
        public string? role {get;set;} 
        
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiMinimal.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace apiMinimal.Context
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext(DbContextOptions options): base(options){}
        public DbSet<Contato> contatos {get;set;}
        public DbSet<User> users {get;set;}
        
    }
}
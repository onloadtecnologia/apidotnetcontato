using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using apiMinimal.Models;
using apidotnetcontato.Dtos;

namespace apidotnetcontato.Authetication
{
    public class JwtAuthenticationService
    {
        public string GeneratedToken(UserDto user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(@"minhachavesupersecreta");

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new[] {
                        new Claim(ClaimTypes.Email,user.email),
                        new Claim(ClaimTypes.Role,user.role),
                        }

                ),
                Expires = DateTime.UtcNow.AddHours(8),
                SigningCredentials =  new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                )
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

        }

                
    }
}